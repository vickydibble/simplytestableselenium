package pages;

import models.User;
import org.openqa.selenium.WebDriver;
import pages.locators.LoginPageLocators;

public class LoginPage {
    private WebDriver driver;
    private LoginPageLocators locators = new LoginPageLocators();
    private String expectedPageTitle = "Sign in";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public Boolean compareExpectedToActualPageTitle() {
        return getPageTitle().contains(expectedPageTitle);
    }

    public void login(User userId) {
        driver.findElement(locators.getEmailFieldLocator()).sendKeys(userId.getEmailAddress());
        driver.findElement(locators.getPasswordFieldLocator()).sendKeys(userId.getPassword());
        driver.findElement(locators.getLoginSubmitButtonLocator()).click();
    }

    public Boolean isEmailNotRegisteredMessageShown() {
        return driver.findElements(locators.getNotRegisteredMessageLocator()).size() != 0;

    }

    public Boolean isRegisterButtonShownInUnregisteredUserLoginAlert() {
        return driver.findElements(locators.getRegisterButtonInNotRegisteredMessageLocator()).size() != 0;

    }
}
