package pages.locators;

import org.openqa.selenium.By;

public class LoginPageLocators {

    private By emailFieldLocator = By.xpath("//input[@id='email']");
    private By passwordFieldLocator = By.xpath("//input[@id='password']");
    private By loginSubmitButtonLocator = By.xpath("//button[@id='submit']");
    private By notRegisteredMessageLocator = By.xpath("//div[contains(@class,'alert')]/p[contains(text(),'no account')]");
    private By registerButtonInNotRegisteredMessageLocator = By.xpath("//button[contains(text(),'sign me up')]");

    public By getNotRegisteredMessageLocator() {
        return notRegisteredMessageLocator;
    }

    public By getEmailFieldLocator() {
        return emailFieldLocator;
    }

    public By getPasswordFieldLocator() {
        return passwordFieldLocator;
    }

    public By getLoginSubmitButtonLocator() {
        return loginSubmitButtonLocator;
    }

    public By getRegisterButtonInNotRegisteredMessageLocator() {
        return registerButtonInNotRegisteredMessageLocator;
    }
}
