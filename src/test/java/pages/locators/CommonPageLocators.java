package pages.locators;

import org.openqa.selenium.By;

public class CommonPageLocators {

    private By signOutButtonLocator = By.xpath("//button[contains(text(),'Sign out')]");
    private By signInButtonLocator = By.xpath("//a[contains(@href,'signin')]");

    public By getSignInButtonLocator() {
        return signInButtonLocator;
    }

    public By getSignOutButtonLocator() {
        return signOutButtonLocator;
    }
}
