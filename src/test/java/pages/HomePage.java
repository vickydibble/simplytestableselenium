package pages;


import org.openqa.selenium.WebDriver;
import pages.locators.HomePageLocators;
import seleniumTests.Utils.Properties;

public class HomePage {

    WebDriver driver;
    HomePageLocators locators = new HomePageLocators();
    String expectedUrl = Properties.getUrl();

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public Boolean compareExpectedToActualUrl() {
        return getUrl().equals(expectedUrl);
    }

}
