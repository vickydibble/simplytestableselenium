package pages;

import org.openqa.selenium.WebDriver;
import pages.locators.CommonPageLocators;


public class CommonPage {

    WebDriver driver;
    CommonPageLocators locators = new CommonPageLocators();

    public CommonPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickSigninButton() {
        driver.findElement(locators.getSignInButtonLocator()).click();
    }

    public boolean isSignOutButtonShown() {
        if (driver.findElements(locators.getSignOutButtonLocator()).size() != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void clickSignOutButton() {
        driver.findElement(locators.getSignOutButtonLocator()).click();
    }


}
