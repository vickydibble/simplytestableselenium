package seleniumTests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import seleniumTests.Utils.Properties;


public class LoginTests extends AbstractBaseTest {

    @BeforeMethod
    public void goToHomePage() {
        driver.get(Properties.getUrl());
        Assert.assertTrue(homePage.compareExpectedToActualUrl());
        if (commonPage.isSignOutButtonShown()) {
            commonPage.clickSignOutButton();
        }
    }

    @Test
    public void successfulLogin() {
        // Registered user can log in with valid details
        commonPage.clickSigninButton();
        Assert.assertTrue(loginPage.compareExpectedToActualPageTitle());
        loginPage.login(userProvider.getUser("RegisteredUser"));
        Assert.assertTrue(commonPage.isSignOutButtonShown());
    }

    @Test
    public void unsuccessfulLoginIncorrectPassword() {
        // Registered User cannot login if their password is incorrect
        commonPage.clickSigninButton();
        Assert.assertTrue(loginPage.compareExpectedToActualPageTitle());
        loginPage.login(userProvider.getUser("RegisteredUserBadPassword"));
        Assert.assertFalse(commonPage.isSignOutButtonShown());

    }

    @Test
    public void unsuccessfulLoginNotRegistered() {
        /* Where an email address is not registered:
        1. They cannot log in
        2. A message shows they are not registered
        3. Link provided for user to go to registration
         */

        commonPage.clickSigninButton();
        Assert.assertTrue(loginPage.compareExpectedToActualPageTitle());
        loginPage.login(userProvider.getUser("unregisteredUser"));
        Assert.assertFalse(commonPage.isSignOutButtonShown());
        Assert.assertTrue(loginPage.isEmailNotRegisteredMessageShown());
        Assert.assertTrue(loginPage.isRegisterButtonShownInUnregisteredUserLoginAlert());
    }

}
