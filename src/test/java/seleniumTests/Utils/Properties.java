package seleniumTests.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Properties {
    private static final String URL = System.getProperty("url", "https://simplytestable.com/");
    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        WebDriver driver;

        if("chrome"==BROWSER) {
            driver = new ChromeDriver();
        }
        else if("firefox"==BROWSER){
            driver = new FirefoxDriver();
        }
        else if("headless"==BROWSER){
            ChromeOptions options= new ChromeOptions();
            options.addArguments("--headless");
            driver = new ChromeDriver(options);
        }
        else {
            driver = new ChromeDriver();
        }
        return driver;
    }
}
