package seleniumTests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.CommonPage;
import pages.HomePage;
import pages.LoginPage;
import seleniumTests.Utils.Properties;
import services.UserProvider;

public abstract class AbstractBaseTest {
    WebDriver driver;
    UserProvider userProvider = new UserProvider();
    CommonPage commonPage;
    HomePage homePage;
    LoginPage loginPage;

    @BeforeClass
    public void setUp(){
        driver = Properties.getBrowser();
        commonPage = new CommonPage(driver);
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }

}
